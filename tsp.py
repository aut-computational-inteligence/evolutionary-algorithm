from Basis.evolutionary_algorithm_functions import *
from Basis.evolutionary_algorithm import EvolutionaryAlgorithm


class TSP:
    def __init__(self, problem_inputs):
        self.number_cites = problem_inputs['number_cites']
        self.x = problem_inputs['x']
        self.y = problem_inputs['y']
        self.problem_inputs = problem_inputs
        self._algorithm4()

    def _algorithm1(self):
        self.solver = EvolutionaryAlgorithm(
            [mutation_swap_tsp, {'t': 0.1}],
            [crossover_order_one, None],
            [parent_selection_random, None],
            [remaining_population_selection_best_children, None],
            evaluator_tsp,
            [gene_generator_tsp, {'initial_mutation_rate': 0.4}],
            [stop_condition_evaluation_count, {"max_evaluation_count": 400000}],
            1000,
            7000,
            self.problem_inputs
        )

    def _algorithm2(self):
        self.solver = EvolutionaryAlgorithm(
            [mutation_swap_tsp, {'t': 0.5}],
            [crossover_cycle, None],
            [parent_selection_random, None],
            [remaining_population_selection_best_children, None],
            evaluator_tsp,
            [gene_generator_tsp, {'initial_mutation_rate': 0.1}],
            [stop_condition_evaluation_count, {"max_evaluation_count": 4000000}],
            1000,
            7000,
            self.problem_inputs
        )

    def _algorithm3(self):
        self.solver = EvolutionaryAlgorithm(
            [mutation_swap_tsp, {'t': 0.5}],
            [crossover_maximal_preservation, None],
            [parent_selection_random, None],
            [remaining_population_selection_best_children, None],
            evaluator_tsp,
            [gene_generator_tsp, {'initial_mutation_rate': 0.1}],
            [stop_condition_evaluation_count, {"max_evaluation_count": 2000000}],
            1000,
            7000,
            self.problem_inputs
        )

    def _algorithm4(self):
        self.solver = EvolutionaryAlgorithm(
            [mutation_swap_tsp, {'t': 0.5}],
            [crossover_order_one, None],
            [parent_selection_random, None],
            [remaining_population_selection_best_children, None],
            evaluator_tsp,
            [gene_generator_tsp, {'initial_mutation_rate': 0.1}],
            [stop_condition_evaluation_count_convergence, {"max_evaluation_count": 3000000, 'sigma_convergence':0.1}],
            1000,
            7000,
            self.problem_inputs
        )

    def _algorithm5(self):
        self.solver = EvolutionaryAlgorithm(
            [mutation_inversion, {'t': 0.5}],
            [crossover_order_one, None],
            [parent_selection_random, None],
            [remaining_population_selection_best_children, None],
            evaluator_tsp,
            [gene_generator_tsp, {'initial_mutation_rate': 0.1}],
            [stop_condition_evaluation_count_convergence, {"max_evaluation_count": 2000000, 'sigma_convergence':0}],
            100,
            700,
            self.problem_inputs
        )

    def solve(self):
        self.solver.run()
        self.solver.best_chromosome().show_phenotype_tsp(self.problem_inputs)
