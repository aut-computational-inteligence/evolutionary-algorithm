from file_interface import FileInterface
from tsp import TSP
from knapsack import Knapsack


def run_tsp():
    file_interface = FileInterface("./data/tsp_data.txt")
    problem_inputs = file_interface.problem_inputs_tsp()
    tsp = TSP(problem_inputs)
    tsp.solve()


def run_knapsack():
    file_interface = FileInterface("./data/knapsack_3.txt")
    problem_inputs = file_interface.problem_inputs_knapsack()
    knapsack = Knapsack(problem_inputs)
    knapsack.solve()


run_tsp()
# run_knapsack()
