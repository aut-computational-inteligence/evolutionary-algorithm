from Basis.chromosome import Chromosome
import random


class EvolutionaryAlgorithm:
    def __init__(self,
                 mutation,
                 cross_over,
                 parent_selection,
                 remaining_population_selection,
                 evaluator,
                 gene_generator,
                 stop_condition,
                 m,
                 y,
                 problem_inputs,
                 ):
        """
        :param problem_inputs: inputs of problem: Dictionary
        :param m: Mu (number of population), number of population, Integer
        :param y: Lambda (number of children), number of children, Integer
        :param mutation: Mutation algorithm, Function
        :param cross_over: Cross over algorithm, Function
        :param parent_selection: Selection algorithm for parents, Function
        :param remaining_population_selection: Selection algorithm for remaining population, Function
        :param evaluator: Evaluator algorithm for each chromosome, Function
        :param gene_generator: Random algorithm for initial population, Function
        :param stop_condition: Stop condition function, Function
        """

        self._mutation = mutation[0]
        self._mutation_params = mutation[1]

        self._cross_over = cross_over[0]
        self._cross_over_params = cross_over[1]

        self._parent_selection = parent_selection[0]
        self._parent_selection_params = parent_selection[1]

        self._remaining_population_selection = remaining_population_selection[0]
        self._remaining_population_selection_params = remaining_population_selection[1]

        self._evaluator = evaluator

        self._gene_generator = gene_generator[0]
        self._gene_generator_params = gene_generator[1]

        self._stop_condition = stop_condition[0]
        self._stop_condition_param = stop_condition[1]

        self._generation_counter = 0
        self._evaluation_counter = 0

        self.problem_inputs = problem_inputs

        self._m = m
        self._y = y
        self._n_parent = y
        best_chromosome_fitness_in_total = -1
        best_phenotype = None
        self._population = []

    def run(self):
        self._initial_population()
        self._generation_counter = 1
        while not self._stop_condition(self._generation_counter, self._evaluation_counter, self._population,
                                       self._stop_condition_param):
            self._generation_counter += 1
            parents = self._parent_selection(self._population, self._y, self._parent_selection_params)
            children = self._new_children(parents)
            # if type(self._population) != list:
            #     self._population = self._population.tolist()
            self._population = self._remaining_population_selection(self._population, children, self._m,
                                                                    self._remaining_population_selection_params)
            self.show_population()

    def _new_children(self, parents):
        children = []
        random.shuffle(parents)
        for i in range(0, len(parents) - 1, 2):
            chromosome1, chromosome2 = self._cross_over(parents[i], parents[i + 1], self.problem_inputs,
                                                        self._cross_over_params)
            chromosome1 = self._mutation(chromosome1, self.problem_inputs, self._mutation_params)
            chromosome2 = self._mutation(chromosome2, self.problem_inputs, self._mutation_params)
            chromosome1.fitness = self._evaluator(chromosome1, self.problem_inputs)
            self._evaluation_counter += 1
            chromosome2.fitness = self._evaluator(chromosome2, self.problem_inputs)
            self._evaluation_counter += 1
            children += [chromosome1, chromosome2]
            if len(children) >= self._y:
                break
        return children[:self._y]

    def show_population(self):
        for i in range(len(self._population)):
            print(self._population[i].fitness, end=',')
        print()

    def best_chromosome(self):
        best = self._population[0]
        for i in range(1, len(self._population)):
            print(self._population[i].fitness, end=',')
            if self._population[i].fitness > best.fitness:
                best = self._population[i]
        return best

    def _initial_population(self):
        for i in range(self._m):
            random_gene = self._gene_generator(self.problem_inputs, self._gene_generator_params)
            chromosome = Chromosome(random_gene, 0)
            chromosome.fitness = self._evaluator(chromosome, self.problem_inputs)
            self._evaluation_counter += 1
            self._population.append(chromosome)
