import numpy as np
import random
import math
from Basis.chromosome import Chromosome


def gen_generator_knapsack(problem_inputs, params=None):
    if params is None:
        params = {'maxim_capacity_scale': 1, 'initial_mutation_rate': 0.1}

    number_items = problem_inputs.get('number_items')
    capacity = problem_inputs.get('capacity')
    weights = problem_inputs.get('weights')
    maxim_capacity = capacity * params['maxim_capacity_scale']
    gene = np.zeros(number_items + 1, dtype=float)
    gene[number_items] = params['initial_mutation_rate']

    while True:
        random_item = random.randrange(0, number_items)
        if weights[random_item] <= maxim_capacity:
            if gene[random_item] == 0:
                maxim_capacity -= weights[random_item]
                gene[random_item] = 1
        else:
            break

    return gene


def mutation_function_zero_one(mutation_rate, k):
    if k >= 0:
        return 2 * (1 - mutation_rate) / math.pi * math.atan(k)
    else:
        return 2 * mutation_rate / math.pi * math.atan(k)


def mutation_unary_knapsack(chromosome, problme_inputs=None, params=None):
    if params is None:
        params = {'t': 0.1}

    mutation_rate = chromosome.mutation_rate()
    new_gene = np.zeros(chromosome.size)

    new_mutation_rate = mutation_rate
    new_mutation_rate += mutation_function_zero_one(mutation_rate, params['t'] * np.random.normal(0, 1))

    new_gene[chromosome.size - 1] = new_mutation_rate

    for i in range(chromosome.size - 1):
        if random.uniform(0, 1) < new_mutation_rate:
            new_gene[i] = 1 - chromosome.genotype[i]
    new_chromosome = Chromosome(new_gene, 0)
    return new_chromosome


def mutation_group_knapsack(chromosome, problem_inputs, params=None):
    if params is None:
        params = {'t': 0.1}

    mutation_rate = chromosome.mutation_rate()
    new_gene = np.zeros(chromosome.size)

    new_mutation_rate = mutation_rate
    new_mutation_rate += mutation_function_zero_one(mutation_rate, params['t'] * np.random.normal(0, 1))

    new_gene[chromosome.size - 1] = new_mutation_rate

    selected = []
    removed = []

    selected_weight = 0
    for i in range(chromosome.size - 1):
        if chromosome.genotype[i] == 1:
            selected.append(i)
            selected_weight += problem_inputs['weights'][i]
        else:
            removed.append(i)

    number_new_items = int(new_mutation_rate * (len(removed)))

    selected_mutation = np.random.choice(removed, size=number_new_items, replace=False).tolist()
    removed = [elem for elem in removed if elem not in selected_mutation]
    random.shuffle(selected)

    for i in selected_mutation:
        selected_weight += problem_inputs['weights'][i]

    while selected_weight > problem_inputs['capacity']:
        if len(selected) == 0:
            break
        index = random.choice(selected)
        selected.remove(index)
        removed.append(index)
        selected_weight -= problem_inputs['weights'][index]

    while selected_weight > problem_inputs['capacity']:
        if len(selected_mutation) == 0:
            break
        index = random.choice(selected_mutation)
        selected_mutation.remove(index)
        removed.append(index)
        selected_weight -= problem_inputs['weights'][index]

    for i in selected:
        new_gene[i] = 1

    for i in selected_mutation:
        new_gene[i] = 1

    new_chromosome = Chromosome(new_gene, 0)
    return new_chromosome


def crossover_uniform_knapsack(parent1, parent2, problem_input, params=None):
    parent1_gen = parent1.genotype
    parent2_gen = parent2.genotype
    size = parent1.size
    child1_gen = np.zeros(size)
    child2_gen = np.zeros(size)

    for i in range(size):
        if random.uniform(0, 1) > 0.5:
            child1_gen[i] = parent1_gen[i]
            child2_gen[i] = parent2_gen[i]
        else:
            child2_gen[i] = parent1_gen[i]
            child1_gen[i] = parent2_gen[i]
    child1 = Chromosome(child1_gen, 0)
    child2 = Chromosome(child2_gen, 0)
    return child1, child2


def crossover_balanced_knapsack(parent1, parent2, problem_input, params=None):
    parent1_gen = parent1.genotype
    parent2_gen = parent2.genotype
    size = parent1.size
    child1_gen = np.zeros(size)
    child2_gen = np.zeros(size)
    child1_remain_capacity = problem_input['capacity']
    child2_remain_capacity = problem_input['capacity']

    order = []
    for i in range(size - 1):
        order.append(i)
    random.shuffle(order)

    for i in order:
        current_weight = problem_input['weights'][i]
        if parent1_gen[i] == 1 and parent2_gen[i] == 1:
            if child1_remain_capacity >= current_weight and child2_remain_capacity >= current_weight:
                child1_remain_capacity -= current_weight
                child2_remain_capacity -= current_weight
                child1_gen[i] = 1
                child2_gen[i] = 1

    for i in order:
        current_weight = problem_input['weights'][i]
        if parent1_gen[i] != parent2_gen[i]:
            if child2_remain_capacity < current_weight <= child1_remain_capacity:
                child1_gen[i] = 1
                child2_gen[i] = 0
                child1_remain_capacity -= current_weight
            elif child1_remain_capacity < current_weight <= child2_remain_capacity:
                child1_gen[i] = 0
                child2_gen[i] = 1
                child2_remain_capacity -= current_weight
            elif child2_remain_capacity < current_weight and child2_remain_capacity < current_weight:
                child1_gen[i] = 0
                child2_gen[i] = 0
            else:
                if random.randrange(0, child1_remain_capacity) > random.randrange(0, child2_remain_capacity):
                    child1_gen[i] = 1
                    child2_gen[i] = 0
                    child1_remain_capacity -= current_weight
                else:
                    child2_gen[i] = 1
                    child1_gen[i] = 0
                    child2_remain_capacity -= current_weight

    for i in order:
        current_weight = problem_input['weights'][i]
        if parent1_gen[i] != parent2_gen[i]:
            if child1_gen[i] == 0 and current_weight < child1_remain_capacity:
                child1_gen[i] = 1
                child1_remain_capacity -= current_weight
            if child2_gen[i] == 0 and current_weight < child2_remain_capacity:
                child2_gen[i] = 1
                child2_remain_capacity -= current_weight

    if random.uniform(0, 1) > 0.5:
        child1_gen[size - 1] = parent1_gen[size - 1]
        child2_gen[size - 1] = parent2_gen[size - 1]
    else:
        child2_gen[size - 1] = parent1_gen[size - 1]
        child1_gen[size - 1] = parent2_gen[size - 1]

    child1 = Chromosome(child1_gen, 0)
    child2 = Chromosome(child2_gen, 0)
    return child1, child2


def parent_selection_random(population, number_parents, parameter=None):
    return np.random.choice(population, size=number_parents)


def evaluator_knapsack(chromosome, problem_inputs):
    number_items = problem_inputs.get('number_items')
    capacity = problem_inputs.get('capacity')
    weights = problem_inputs.get('weights')
    values = problem_inputs.get('values')
    genotype = chromosome.genotype
    sum_values = 0
    sum_weights = 0
    for i in range(number_items):
        if genotype[i] == 1:
            sum_values += values[i]
            sum_weights += weights[i]
    if sum_weights > capacity:
        return 0
    else:
        return sum_values


def remaining_population_selection_best_children(population, children, m, params=None):
    sorted_list = sorted(children, key=lambda x: -x.fitness)
    return sorted_list[:m]


def remaining_population_selection_best_all(population, children, m, params=None):
    sorted_list = sorted(population + children, key=lambda x: -x.fitness)
    return sorted_list[:m]


def stop_condition_generation(generation, evaluation_count, population, params=None):
    max_generation = params['max_generation']
    if generation < max_generation:
        return False
    return True


def stop_condition_evaluation_count(generation, evaluation_count, population, params=None):
    max_evaluation_count = params['max_evaluation_count']
    if evaluation_count < max_evaluation_count:
        return False
    return True


def stop_condition_evaluation_count_convergence(generation, evaluation_count, population, params=None):
    max_evaluation_count = params['max_evaluation_count']
    sigma_convergence = params['sigma_convergence']
    if evaluation_count < max_evaluation_count:
        fitness = []
        for p in population:
            fitness.append(p.fitness)
        if np.var(fitness) < sigma_convergence:
            return True
        else:
            return False
    return True


def gene_generator_tsp(problem_inputs, params=None):
    if params is None:
        params = {'initial_mutation_rate': 0.1}

    gen = np.arange(0, problem_inputs['number_cites'])
    random.shuffle(gen)
    gen = np.concatenate((gen, [params['initial_mutation_rate']]))
    return gen.tolist()


def mutation_swap_tsp(chromosome, problem_inputs=None, params=None):
    if params is None:
        params = {'t': 0.1}

    mutation_rate = chromosome.mutation_rate()
    new_mutation_rate = mutation_rate
    new_mutation_rate += mutation_function_zero_one(mutation_rate, params['t'] * np.random.normal(0, 1))

    if np.random.random() <= new_mutation_rate:
        idx = np.random.choice(np.arange(len(chromosome.genotype) - 1), 2, replace=False)
        chromosome.genotype[idx[0]], chromosome.genotype[idx[1]] = \
            chromosome.genotype[idx[1]], chromosome.genotype[idx[0]]
    chromosome.genotype[-1] = new_mutation_rate
    return chromosome


def mutation_multi_swap_tsp(chromosome, problem_inputs=None, params=None):
    if params is None:
        params = {'t': 0.1}

    mutation_rate = chromosome.mutation_rate()
    new_mutation_rate = mutation_rate
    new_mutation_rate += mutation_function_zero_one(mutation_rate, params['t'] * np.random.normal(0, 1))

    for idx1 in range(chromosome.size-2):
        if np.random.random() <= new_mutation_rate:
            idx2 = random.randrange(idx1, chromosome.size-1)
            chromosome.genotype[idx1], chromosome.genotype[idx2] = \
                chromosome.genotype[idx2], chromosome.genotype[idx1]

    chromosome.mutation_rate = new_mutation_rate
    return chromosome


def mutation_inversion(chromosome, problem_inputs=None, params=None):
    if params is None:
        params = {'t': 0.1}

    mutation_rate = chromosome.mutation_rate()
    new_mutation_rate = mutation_rate
    new_mutation_rate += mutation_function_zero_one(mutation_rate, params['t'] * np.random.normal(0, 1))
    chromosome.genotype[-1] = new_mutation_rate
    if random.uniform(0, 1) > new_mutation_rate:
        cut_points = np.sort(
            np.random.choice(np.arange(len(chromosome.genotype)-1), replace=False, size=2))

        tmp_gen = np.concatenate((chromosome.genotype[:cut_points[0]],
                                  chromosome.genotype[cut_points[1] + 1:]))

        insert_point = np.random.choice(np.arange(len(tmp_gen) + 1), size=1)[0]
        cut = chromosome.genotype[cut_points[0]:cut_points[1] + 1]
        reversed_cut = cut[::-1]

        chromosome.genotype = np.concatenate((tmp_gen[:insert_point],
                                              reversed_cut,
                                              tmp_gen[insert_point:]))

    return chromosome


def crossover_order_one(parent1, parent2, problem_inputs=None, params=None):
    parent1_gen = parent1.genotype[:-1]
    parent2_gen = parent2.genotype[:-1]
    n = len(parent1_gen)
    start_substr = random.randint(0, n - 2)
    end_substr = random.randint(start_substr + 1, n - 1)
    child1 = parent1.genotype.copy()
    child2 = parent2.genotype.copy()
    j = end_substr
    i = end_substr
    while j != start_substr:
        if not parent1_gen[i] in child2[start_substr:end_substr]:
            child2[j] = parent1_gen[i]
            j = (j + 1) % n
        i = (i + 1) % n

    j = end_substr
    i = end_substr
    while j != start_substr:
        if not parent2_gen[i] in child1[start_substr:end_substr]:
            child1[j] = parent2_gen[i]
            j = (j + 1) % n
        i = (i + 1) % n
    return Chromosome(child1, 0), Chromosome(child2, 0)


def find_cycle(parent1_gen, parent2_gen, cycle):
    last_idx = cycle[-1]
    value_finding = parent1_gen[last_idx]
    new_idx = parent2_gen.index(value_finding)
    if new_idx == cycle[0]:
        return cycle
    cycle.append(new_idx)
    return find_cycle(parent1_gen, parent2_gen, cycle)


def crossover_cycle(parent1, parent2, problem_inputs=None, parameters=None):
    child1 = np.empty(parent1.size)
    child1.fill(-1.0)
    child2 = np.empty(parent1.size)
    child2.fill(-1.0)

    parent1_gen = parent1.genotype[:-1]
    parent2_gen = parent2.genotype[:-1]

    n = len(parent1_gen)
    start_index = 0
    parent_order = True
    while start_index < n:
        if child1[start_index] == -1:
            cycle = find_cycle(parent1_gen, parent2_gen, [start_index])
            for i in cycle:
                if parent_order:
                    child1[i] = parent1_gen[i]
                    child2[i] = parent2_gen[i]
                else:
                    child2[i] = parent1_gen[i]
                    child1[i] = parent2_gen[i]
            parent_order = not parent_order
        else:
            start_index += 1

    if parent_order:
        child1[n] = parent1.genotype[-1]
        child2[n] = parent2.genotype[-1]
    else:

        child2[n] = parent1.genotype[-1]
        child1[n] = parent2.genotype[-1]

    return Chromosome(child1, 0), Chromosome(child2, 0)


def crossover_maximal_preservation(parent1, parent2, problem_inputs=None, parameters=None):
    """
    :param parameters: dictionary of parameters that key = parameter name and value = parameter value
    :param parent1: First parent chromosome, Gene, np.array with shape (1, _n)
    :param parent2: Second parent chromosome, Gene, np.array with shape (1, _n)
    :return: return two chromosome for each children, Chromosome
    """

    parent1_genotype = parent1.genotype[:-1]
    parent2_genotype = parent2.genotype[:-1]
    n = parent1.size-1

    rand_len = random.choice(list(range(2, len(parent1_genotype) // 2)))
    rand_start_index = random.choice(list(range(0, len(parent1_genotype) - 2)))

    child1_genotype = np.zeros(n+1)
    child2_genotype = np.zeros(n+1)

    for i in range(rand_len):
        child1_genotype[i] = parent1_genotype[(rand_start_index+i)%n]
        child2_genotype[i] = parent2_genotype[(rand_start_index+i)%n]

    index = rand_len
    for i in range(n):
        value = parent2_genotype[i]
        if not (value in child1_genotype[0:index]):
            child1_genotype[index] = value
            index += 1

    index = rand_len
    for i in range(n):
        value = parent1_genotype[i]
        if not (value in child2_genotype[0:index]):
            child2_genotype[index] = value
            index += 1

    child1_genotype[-1] = parent1.genotype[-1]
    child2_genotype[-1] = parent2.genotype[-1]

    child1 = Chromosome(child1_genotype, 0)
    child2 = Chromosome(child2_genotype, 0)
    return child1, child2


def evaluator_tsp(chromosome, problem_inputs):
    sum_distance = 0.0
    genotype = chromosome.genotype
    x = problem_inputs['x']
    y = problem_inputs['y']
    for i in range(problem_inputs['number_cites'] - 1):
        index1 = int(genotype[i])
        index2 = int(genotype[i + 1])
        current_distance = math.sqrt(pow(x[index1] - x[index2], 2) + pow(y[index1] - y[index2], 2))
        sum_distance += current_distance

    index1 = int(genotype[0])
    index2 = int(genotype[problem_inputs['number_cites'] - 1])
    sum_distance += math.sqrt(pow(x[index1] - x[index2], 2) + pow(y[index1] - y[index2], 2))
    return -sum_distance
