import matplotlib.pyplot as plt


class Chromosome:
    def __init__(self, genotype, fitness):
        self.fitness = fitness
        if type(genotype) is not list:
            self.genotype = genotype.tolist()
        else:
            self.genotype = genotype
        self.size = len(genotype)

    def mutation_rate(self):
        return self.genotype[self.size-1]

    def show_phenotype_knapsack(self, problem_inputs):
        print("\n\n\n==========")
        print("Genotype: ", self.genotype)
        print("values: ", self.fitness)
        sum_weight = 0
        print("Selected items:")
        for i in range(len(self.genotype) - 1):
            if self.genotype[i] == 1:
                print(problem_inputs['values'][i], problem_inputs['weights'][i])
                sum_weight += problem_inputs['weights'][i]
        print("weights: ", sum_weight, "of ", problem_inputs['capacity'])
        print("==========")

    def show_phenotype_tsp(self, problem_inputs):
        print("\n\n\n==========")
        print("Genotype: ", self.genotype)
        print("Distance: ", -1*self.fitness)
        x = problem_inputs['x']
        y = problem_inputs['y']
        plt.scatter(x, y)
        print("==========")
        for i in range(problem_inputs['number_cites']-1):
            index1 = int(self.genotype[i])
            index2 = int(self.genotype[i+1])
            line_x = [x[index1], x[index2]]
            line_y = [y[index1], y[index2]]
            plt.plot(line_x, line_y, color='red')
        index1 = int(self.genotype[problem_inputs['number_cites']-1])
        index2 = int(self.genotype[0])
        line_x = [x[index1], x[index2]]
        line_y = [y[index1], y[index2]]
        plt.plot(line_x, line_y, color='red')
        plt.show()
