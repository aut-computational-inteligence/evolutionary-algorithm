class FileInterface:
    def __init__(self, file_address):
        self.file = open(file_address, 'r')

    def problem_inputs_knapsack(self):
        number_items, capacity = [int(x) for x in self.file.readline().split()]
        weights = []
        values = []
        for i in range(number_items):
            new_value, new_weight = [int(x) for x in self.file.readline().split()]
            values.append(new_value)
            weights.append(new_weight)
        problem_inputs = {'number_items': number_items, 'capacity': capacity,
                          'weights': weights, 'values': values}
        return problem_inputs

    def problem_inputs_tsp(self):
        number_cites = 0
        x = []
        y = []
        for line in self.file:
            number_cites, new_y, new_x = [float(x) for x in line.split()]
            x.append(new_x)
            y.append(new_y)
        problem_inputs = {'number_cites': int(number_cites), 'x': x, 'y': y}
        return problem_inputs

