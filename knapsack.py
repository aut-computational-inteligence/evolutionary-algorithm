from Basis.evolutionary_algorithm import EvolutionaryAlgorithm
from Basis.evolutionary_algorithm_functions import *


class Knapsack:
    def __init__(self, problem_inputs):
        self.number_items = problem_inputs['number_items']
        self.capacity = problem_inputs['capacity']
        self.values = problem_inputs['values']
        self.weights = problem_inputs['weights']
        self.problem_inputs = problem_inputs
        self._algorithm4()

    def _algorithm1(self):
        self.solver = EvolutionaryAlgorithm(
            [mutation_unary_knapsack, {'t': 0.2}],
            [crossover_uniform_knapsack, None],
            [parent_selection_random, None],
            [remaining_population_selection_best_children, None],
            evaluator_knapsack,
            [gen_generator_knapsack, {'maxim_capacity_scale': 1, 'initial_mutation_rate': 0.2}],
            [stop_condition_evaluation_count, {"max_evaluation_count": 100000}],
            500,
            3500,
            self.problem_inputs
        )

    def _algorithm2(self):
        self.solver = EvolutionaryAlgorithm(
            [mutation_unary_knapsack, {'t': 0.1}],
            [crossover_balanced_knapsack, None],
            [parent_selection_random, None],
            [remaining_population_selection_best_children, None],
            evaluator_knapsack,
            [gen_generator_knapsack, {'maxim_capacity_scale': 1, 'initial_mutation_rate': 0.1}],
            [stop_condition_evaluation_count, {"max_evaluation_count": 800000}],
            1000,
            7000,
            self.problem_inputs
        )

    def _algorithm3(self):
        self.solver = EvolutionaryAlgorithm(
            [mutation_group_knapsack, {'t': 0.1}],
            [crossover_balanced_knapsack, None],
            [parent_selection_random, None],
            [remaining_population_selection_best_children, None],
            evaluator_knapsack,
            [gen_generator_knapsack, {'maxim_capacity_scale': 1, 'initial_mutation_rate': 0.01}],
            [stop_condition_evaluation_count, {"max_evaluation_count": 100000}],
            1000,
            7000,
            self.problem_inputs
        )

    def _algorithm4(self):
        self.solver = EvolutionaryAlgorithm(
            [mutation_group_knapsack, {'t': 0.1}],
            [crossover_balanced_knapsack, None],
            [parent_selection_random, None],
            [remaining_population_selection_best_children, None],
            evaluator_knapsack,
            [gen_generator_knapsack, {'maxim_capacity_scale': 1, 'initial_mutation_rate': 0.01}],
            [stop_condition_evaluation_count_convergence, {'max_evaluation_count': 100000, 'sigma_convergence': 5}],
            1000,
            7000,
            self.problem_inputs
        )

    def solve(self):
        self.solver.run()
        self.solver.best_chromosome().show_phenotype_knapsack(self.problem_inputs)

